import 'package:flutter/material.dart';
import 'package:uefa/HomePage/export.dart';
import 'package:uefa/models/tournament_model.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List tournamentsList = [
    Tournament(
        imageUrl:
            'https://flutter-examples.com/wp-content/uploads/2019/09/blossom.jpg',
        stadiumName: "Al Warah Stadium",
        opponents: "Al Warah  SC vs AL Khor SC",
        hostDate: DateTime.now().toString()),
    Tournament(
        imageUrl:
            'https://flutter-examples.com/wp-content/uploads/2019/09/blossom.jpg',
        stadiumName: "Al Khor Stadium",
        opponents: "Al Warah  SC vs AL Rayyan",
        hostDate: DateTime.now().toString()),
    Tournament(
        imageUrl:
            'https://flutter-examples.com/wp-content/uploads/2019/09/blossom.jpg',
        stadiumName: "Al Janoub Stadium",
        opponents: "Uefa Nations League",
        hostDate: DateTime.now().toString()),
    Tournament(
        imageUrl:
            'https://flutter-examples.com/wp-content/uploads/2019/09/blossom.jpg',
        stadiumName: "Al Khor Stadium",
        opponents: "Al Warah  SC vs AL Rayyan",
        hostDate: DateTime.now().toString())
  ];
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 4,
        child: Scaffold(
            body: TabBarView(
              children: [
                homePageLayout(),
                Icon(Icons.person_add),
                Icon(Icons.receipt),
                Icon(Icons.event),
              ],
            ),
            bottomNavigationBar: TabBar(
              labelColor: Colors.black,
              tabs: [
                Tab(
                  icon: Icon(
                    Icons.event,
                    color: Colors.black,
                  ),
                ),
                Tab(
                  icon: Icon(Icons.person_add,
                  color: Colors.black,),
                ),
                Tab(
                  icon: Icon(Icons.receipt,
                  color: Colors.black,),
                ),
                Tab(
                  icon: Icon(Icons.event),
                )
              ],
            )
            ));
  }

  Widget homePageLayout() {
    return Stack(children: <Widget>[
      Container(
        height: MediaQuery.of(context).size.height * 0.3,
        decoration: BoxDecoration(
            color: Colors.blue,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(40),
              bottomRight: Radius.circular(40),
            )),
      ),
      Container(
          color: Colors.transparent,
          margin:
              EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.2),
          child: Scrollbar(
            child: ListView(
                children: tournamentsList
                    .map((e) => TournamentDetails(
                          details: e,
                        ))
                    .toList()),
          )),
      Positioned(
        top: 0.0,
        left: 0.0,
        right: 0.0,
        child: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [Icon(Icons.arrow_back), Text("Events")],
          ),
        ),
      ),
    ]);
  }
}

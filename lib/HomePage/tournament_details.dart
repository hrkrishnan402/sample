import 'package:flutter/material.dart';
import 'package:uefa/models/tournament_model.dart';

class TournamentDetails extends StatefulWidget {
  Tournament details;
  TournamentDetails({
    Key key,
    this.details,
  }) : super(key: key);

  @override
  _TournamentDetailsState createState() => _TournamentDetailsState();
}

class _TournamentDetailsState extends State<TournamentDetails> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 20.0),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        child: ListTile(
          leading: ClipRRect(
            borderRadius: BorderRadius.circular(8.0),
            child: Image.network(widget.details.imageUrl,
                width: 50, height: 100, fit: BoxFit.fitHeight),
          ),
          trailing: CircleAvatar(
            backgroundColor: Colors.blue,
            child: Icon(Icons.receipt),
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              listTileDetails(widget.details.stadiumName, Icons.location_pin),
              listTileDetails(widget.details.opponents, Icons.blur_circular),
              Row(
                children: [
                  Icon(Icons.date_range),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Reported AI:",
                        style: TextStyle(fontSize: 10),
                      ),
                      Text(widget.details.hostDate,
                          style: TextStyle(fontSize: 10)),
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget listTileDetails(String text, IconData icon) {
    return Row(
      children: [
        Icon(icon),
        Text(text, style: TextStyle(fontSize: 10)),
      ],
    );
  }
}

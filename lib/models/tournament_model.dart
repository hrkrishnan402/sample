class Tournament{
  final String imageUrl;
  final String stadiumName;
  final String opponents;
  final String hostDate;

  Tournament({this.imageUrl, this.stadiumName, this.opponents, this.hostDate});
}